= Communication and Meetings

Fedora Docs currently holds weekly meetings on IRC/Telegram/Matrix.
See the link:++https://calendar.fedoraproject.org//docs/++[docs calendar] for current meeting times,
and xref:index.adoc#find-docs[the main page] for information about joining.

This page describes the structure in the meetings.

== Pre-meeting

Items to be discussed in meetings should be tagged with the link:++https://gitlab.com/groups/fedora/docs/-/issues?label_name=Meeting++[Meeting] tag.
While preparing the agenda, review the https://gitlab.com/groups/fedora/docs/-/boards[issue board] for items that should be tagged for the meeting.
At least a day or two prior to the meeting, the chair should post a thread with the agenda
on the link:++https://discussion.fedoraproject.org/tag/docs++[forums].
Team members may suggest additional topics in these threads.

== Meeting

Follow this general script for the meeting.

```
#startmeeting docs

#topic Roll call
```

Wait 3–5 minutes to see who is present.

TIP: In case you need to step away or get disconnected, add other chairs (e.g. xref:index.adoc#_the_board[board members]) with: #chair <NICK>

List the agenda for the meeting:

```
#topic Agenda
#info Announcements
#info Review action items
#info <topic 1>
#info <topic 2>
#info …
```

Start with any important announcements.
(e.g. Release Notes that need to be written, upcoming deadlines, a link to the https://gitlab.com/groups/fedora/docs/-/boards[issue board], etc.)

Next, review action items from https://meetbot.fedoraproject.org/sresults/?group_id=docs&type=team[previous meetings].
Get updates when there are updates to get.
If an action item is still open,
use the `#action` command to assign it again.
This keeps chairs from having to search many previous meetings.

```
#topic Previous action items
#info <nick> was to <action>
```

Proceed through the other agenda topics.
Use `#topic` to change the topic.
Make frequent use of `#info` and `#link` commands to add context to the minutes.
If someone needs to act,
use `#action <nick> to <action>` to note it in the minutes.
Note agreements with the `#agreed` command.

TIP: Most of the time,
something that is #agreed should be recorded elsewhere after the meeting.
For example,
in the resolution of a ticket
or as a new policy/procedure in the documentation.

NOTE: Should we add a regular content-related topic?
https://discussion.fedoraproject.org/t/fedora-and-centos-docs-revitalization/36353/7[Peter Boy suggested this].

If time permits,
open the floor for other discussion.

```
#topic Open floor
```

CAUTION: Do not make decisions in the open floor section.
Decisions should,
at a minimum,
be made after appearing on the meeting agenda in advance.
In most cases, decisions should happen asynchronously.

When time is up or there are no more topics to discuss, end the meeting.

```
#endmeeting
```

== Post-meeting

Paste the links to the minutes and logs in the Discussion thread you used for the agenda.
Note any important outcomes.
Update tickets or discussion threads with actions, agreements, etc.
